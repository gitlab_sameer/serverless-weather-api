# import required modules
import requests
import re
import json
import boto3
from botocore.exceptions import ClientError

def get_secret():
    """ This function retrieves api secret key from secrets manager """

    secret_name = "slalom_demo_key"
    region_name = "us-east-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
        res = get_secret_value_response['SecretString']
        # seperate the actual api key value from the string
        st = res.split(':')[1]
        # use regex and re module to extract the actual api key ( with .group() returns the api key)
        secret = re.search(r'\w+', st)
        return secret.group()
    
    except ClientError as e:
        # #For a list of exceptions thrown, see
        # https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        raise e
    

def get_coordinates(key,api_data):
    """ This function gets the gps coorinates of the city based on zip code and country code povided """

    cc = 'US'
    zip = api_data

    base_url = 'https://api.openweathermap.org/geo/1.0/zip?' 

    # complete url
    full_url = base_url + 'zip=' + zip + ',' + cc + '&appid=' + key

    # make api call
    response = requests.get(full_url)

    # parse json response
    jsonResponse = response.json()

    # get latitude and longitude
    latitude = jsonResponse['lat']
    longitude = jsonResponse['lon']
    area = jsonResponse['name']

    return latitude, longitude, area

def get_weather(key ,lat, lon):
    """ This function gets the actual weather data fo the given coordinates """
    
    # base_url variable to store url
    base_url = "https://api.openweathermap.org/data/3.0/onecall?"
    
    # complete url address
    complete_url = base_url + "lat=" + str(lat) + "&lon=" + str(lon) + "&units=imperial" + "&&appid=" + key
    
    # return response object
    response = requests.get(complete_url)
    
    return response

def write_to_dynamodb(data):
    """ This function writes the json data to dynamodb table"""

    dynamodb = boto3.resource('dynamodb')
    #table name
    table = dynamodb.Table('Weather')

    #insert data into table
    table.put_item(Item=data)

def lambda_handler(event, context):
    """ main lambda handler, calls all the functions to build the logic """
    # get the api key
    key = get_secret()

    #get city zip and country code data from api gateway
    api_data = event.get('zip')

    # call the coordinates function
    res = get_coordinates(key,api_data)
    lat = res[0]
    lon = res[1]
    area = res[2]

    # call the weather function
    resp = get_weather(key, lat, lon)
    # get the resonse in json format
    body = resp.json()

    #create a json body
    body = {
        'Area': area,
        'Temperature': str(body['current']['temp']),
        'Pressure': str(body['current']['pressure']),
        'Descripion': body['current']['weather'][0]['description'],
        'WindSpeed': str(body['current']['wind_speed']),
        'Humidity': str(body['current']['humidity'])
    }

    #write data into dynamodb table
    write_to_dynamodb(body)

    return json.dumps(body)
