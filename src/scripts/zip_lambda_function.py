#! /usr/bin/python3
"""
This script takes all python files from src/lambda and creates a zip archieve
"""
import os
import sys
from zipfile import ZipFile

#global variable
SHA = os.environ.get('CI_COMMIT_SHA')

def get_file_name():
    """ This funcion scans the directory with .py extension and extracts file names without .py extension """
    file_name = []
    for filename in os.listdir("src/lambda_code"):
        if filename.endswith(".py"):
            name,_ = filename.split('.'[0])
            file_name.append(name)
    return file_name        

def zip_lambda(files):
    """ This function iterates over the list of exracted files and does format and zip operation  """
    all_files = files

    #set current working dir
    current_dir = os.getcwd()

    # change fir to lambda dir
    os.chdir('src/lambda_code')

    for file in all_files:
        #format file names
        format_name = f"{file}"
        temp_path = f"{file}.py"  

        #create zip file with formatted name for each file in dir
        with ZipFile(format_name + '.zip', 'w') as zipObj:
            zipObj.write(temp_path)

        # chaneg back to current working dir
        os.chdir(current_dir)    

def set_env(files):
    """ Tis functio creates a list of env variables based on all the files """    
    all_files = files
    env = []
    for file in all_files:
        # remove '_' from the file name to match the parameter names in main_resources.yml
        name = file.replace('_', '')

        #set the env variable with each file name
        temp = f"{file}.zip"
        passed_env = " " + name + "=" + temp
        env.append(passed_env)
    return env    


def main():
    """ this function clls required functions"""  
    # call function to get the list of all the python files
    all_files = get_file_name()

    #call the iterator function
    zip_lambda(all_files)

    #call set env function
    get_env = set_env(all_files)

    #set CFNPARAM with full path of environment variables
    print(' '.join(get_env))

if __name__ == "__main__":
    main()    