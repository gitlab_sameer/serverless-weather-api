#! /usr/bin/python3
import glob
import boto3
import argparse
from botocore.exceptions import ClientError

def upload_s3(bucket,prefix):
    """ upload zipped file to s3 bucket"""
    try:
        #get the list of zip files
        files = glob.glob("src/lambda/*.zip")
    except OSError as e:
        # throw erro if fils are not present
        raise e    
    
    # create s3 bucket resource
    resource = boto3.client('s3')

    # Upload zipe file to s3 one by one
    try:
        for file in files:
            nfile = file.split("/")[-1]
            resource.upload_file(
                Filename = file,
                Bucket = bucket,
                Key = prefix+"/"+nfile
            )
    except ClientError as e:
        raise e        
    
def main():
    """ this function clls required functions"""  
    #generate args
    parser = argparse.ArgumentParser(description='A script to template')  
    parser.add_argument('--bucket', type=str, nargs=1,
                        help='S3 bucket to upload files to')
    parser.add_argument('--prefix', type=str, nargs=1,
                        help='folder inside S3 bucket ')
    args = parser.parse_args()
    bucket = args.bucket[0]
    prefix = args.prefix[0]

    #call upload function
    upload_s3(bucket,prefix)

if __name__ == "__main__":
    main()    